distance<-function(x,y){
    return(sqrt(sum((x-y)**2)))
}


dist.voisins<-function(vecteur,data){
    return(sapply(1:nrow(data),function(i)distance(vecteur,data[i,-ncol(data)])))
}

kppv<-function(vecteur,k,data){
    distances<-dist.voisins(vecteur,data)
    return(order(distances)[1:k])
}

classerKPPV<-function(vecteur,k,data){
    voisins<-kppv(vecteur,k,data)
    classes<-data[voisins,ncol(data)]
    return(as.numeric(names(which.max(table(classes)))))
}

erreurKPPV<-function(k,data){
    predictions<-sapply(1:nrow(data),function(i)classerKPPV(data[i,-ncol(data)],k,data[-i,]));
    return(1-sum(predictions==data[,ncol(data)])/length(predictions))
}