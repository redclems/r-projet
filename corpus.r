library(NLP)
library(tm)

source("help.r")
source("classifier.r")


nettoyerCorpus<-function(corpus, colCorpus=FALSE){
    corpus<-tm_map(corpus, content_transformer(tolower))
    corpus<-tm_map(corpus, removeNumbers)
    corpus<-tm_map(corpus, removeWords, stopwords("en"))
    corpus<-tm_map(corpus, removePunctuation, preserve_intra_word_dashes = TRUE)
    corpus<-tm_map(corpus, stemDocument)
    corpus<-tm_map(corpus, stripWhitespace)
    
    return(corpus)
}

recupereBalise<-function(corpus, colCorpus=FALSE){


  
  return(corpus)
}

normaliserCorpus<-function(corpus) {
    dataScaled<-scale(corpus[,-ncol(corpus)])
    dataScaled<-cbind(dataScaled, corpus[,ncol(corpus)])
    return(dataScaled)
}

genererCorpus<-function(lowfreq = 200, dirsource = "training", balise=FALSE) {
    training<-VCorpus(DirSource(dirsource, recursive = TRUE), readerControl = list(language = "en"))
    if(balise){
      trainingClean <- nettoyage2(training)#enleve les balise
    }else{
      trainingClean <- nettoyage(training)#garde les balise
    }

    trainingClean <- nettoyerCorpus(trainingClean)

    matTraining <- DocumentTermMatrix(trainingClean)

    motsFrequents <- findFreqTerms(matTraining, lowfreq = lowfreq)
    

    matTraining <- DocumentTermMatrix(trainingClean, control = list(dictionary = motsFrequents))

    data <- as.matrix(matTraining)

    classes <- c(
        rep(1, length(training)/7), 
        rep(2, length(training)/7), 
        rep(3, length(training)/7),
        rep(4, length(training)/7),
        rep(5, length(training)/7),
        rep(6, length(training)/7),
        rep(7, length(training)/7)
    )

    data <- cbind(data, classes)
    return(data)
}

testStatKPPV<-function(lowfreq, kmin = 1, kmax = 5) {
  corpus <- genererCorpus(lowfreq = lowfreq)
  
  stat<-sapply(kmin:kmax, erreurKPPV, data=corpus)
  
  return(stat)
}

#stat<-sapply(100:800, testStatKPPV)
